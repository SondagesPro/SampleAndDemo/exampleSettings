# ExampleSettings

A LimeSurvey plugin to show all available settings and some trick

## Home page & Copyright
- HomePage <http://sondages.pro/>
- Copyright ©2015-2024 Denis Chenu <https://sondages.pro>
- Licence : MIT <https://opensource.org/license/MIT/>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
